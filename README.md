# SQL Database case study using Microsoft Access. #
Database design for a real estate agency in order to store a catalog of all available properties and their features. 

* The database also lists all properties which are no more available on market. (e.g. has been sold, has been let or cancelled)
* It contains buyer’s information (e.g. preferences, price range, personal information) and also seller’s information (e.g. offers, personal information). 
* tracks which seller’s offer can be match to which buyer’s preference. If there is no exact match, it will offer the closest one
* lists agent efficiency and personal information such as name, phone number
* tracks customer feedbacks
* The database doesn't track agent’s salary or bills

-bbordas