USE [10339054]
GO
/****** Object:  Table [dbo].[Agents]    Script Date: 13/05/2016 11:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Agents](
	[AgentID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](25) NOT NULL,
	[Phone] [int] NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[LocationID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AgentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Buyers]    Script Date: 13/05/2016 11:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Buyers](
	[BuyerID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](25) NOT NULL,
	[Phone] [int] NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[AgentID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[BuyerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contract]    Script Date: 13/05/2016 11:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contract](
	[ContractID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](10) NOT NULL,
	[Date] [date] NOT NULL,
	[EndDate] [date] NULL,
	[PropertyID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ContractID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Locations]    Script Date: 13/05/2016 11:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Locations](
	[LocationID] [int] IDENTITY(1,1) NOT NULL,
	[Location] [nvarchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[LocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Properties]    Script Date: 13/05/2016 11:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Properties](
	[PropertyID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](10) NOT NULL,
	[AddressLine1] [nvarchar](25) NOT NULL,
	[AddressLine2] [nvarchar](25) NULL,
	[AddressLine3] [nvarchar](25) NULL,
	[NoOfRooms] [int] NOT NULL,
	[NoOfBaths] [int] NULL,
	[SquareMeters] [int] NULL,
	[Parking] [bit] NULL,
	[Price] [money] NOT NULL,
	[Deposit] [money] NULL,
	[Description] [varchar](250) NULL,
	[SellerID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[PropertyType] [nvarchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[PropertyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sellers]    Script Date: 13/05/2016 11:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sellers](
	[SellerID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](25) NOT NULL,
	[Phone] [int] NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[AgentID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SellerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Agents] ON 

INSERT [dbo].[Agents] ([AgentID], [FirstName], [LastName], [Phone], [Email], [LocationID]) VALUES (9, N'Dana', N'Scully', 672728, N'DS@gmail.com', 1)
INSERT [dbo].[Agents] ([AgentID], [FirstName], [LastName], [Phone], [Email], [LocationID]) VALUES (10, N'Fox', N'Murder', 765456, N'FX@gmail.com', 2)
INSERT [dbo].[Agents] ([AgentID], [FirstName], [LastName], [Phone], [Email], [LocationID]) VALUES (11, N'James', N'Bond', 987654, N'JB@gmail.com', 3)
INSERT [dbo].[Agents] ([AgentID], [FirstName], [LastName], [Phone], [Email], [LocationID]) VALUES (12, N'Melissa', N'McCarty', 564321, N'MM@gmail.com', 4)
INSERT [dbo].[Agents] ([AgentID], [FirstName], [LastName], [Phone], [Email], [LocationID]) VALUES (13, N'Tommy', N'Lee Johns', 452627, N'TLJ@gmail.com', 1)
INSERT [dbo].[Agents] ([AgentID], [FirstName], [LastName], [Phone], [Email], [LocationID]) VALUES (14, N'Will', N'Smith', 652627, N'WS@gmail.com', 2)
INSERT [dbo].[Agents] ([AgentID], [FirstName], [LastName], [Phone], [Email], [LocationID]) VALUES (15, N'Evelyn', N'Salt', 899009, N'ES@gmail.com', 2)
SET IDENTITY_INSERT [dbo].[Agents] OFF
SET IDENTITY_INSERT [dbo].[Buyers] ON 

INSERT [dbo].[Buyers] ([BuyerID], [FirstName], [LastName], [Phone], [Email], [AgentID]) VALUES (1, N'Jason', N'Statam', 8238239, N'JS@gmail.com', 9)
INSERT [dbo].[Buyers] ([BuyerID], [FirstName], [LastName], [Phone], [Email], [AgentID]) VALUES (2, N'Jim', N'Morisson', 6737823, N'JM@gmail.com', 10)
INSERT [dbo].[Buyers] ([BuyerID], [FirstName], [LastName], [Phone], [Email], [AgentID]) VALUES (3, N'Jane', N'Fonda', 7623890, N'JF@gmail.com', 11)
INSERT [dbo].[Buyers] ([BuyerID], [FirstName], [LastName], [Phone], [Email], [AgentID]) VALUES (4, N'Julien', N'Moore', 7638399, N'JM@gmail.com', 12)
INSERT [dbo].[Buyers] ([BuyerID], [FirstName], [LastName], [Phone], [Email], [AgentID]) VALUES (5, N'Jodie', N'Foster', 7665342, N'JF@gmail.com', 13)
INSERT [dbo].[Buyers] ([BuyerID], [FirstName], [LastName], [Phone], [Email], [AgentID]) VALUES (6, N'Michael', N'Jackson', 7627828, N'MJ@gmail.com', 14)
INSERT [dbo].[Buyers] ([BuyerID], [FirstName], [LastName], [Phone], [Email], [AgentID]) VALUES (7, N'Janet', N'Jackson', 8383892, N'JJ@gmail.com', 14)
INSERT [dbo].[Buyers] ([BuyerID], [FirstName], [LastName], [Phone], [Email], [AgentID]) VALUES (8, N'Holly', N'Crap', 7634783, N'HC', 12)
SET IDENTITY_INSERT [dbo].[Buyers] OFF
SET IDENTITY_INSERT [dbo].[Contract] ON 

INSERT [dbo].[Contract] ([ContractID], [Type], [Date], [EndDate], [PropertyID]) VALUES (1, N'Sold', CAST(N'2015-01-01' AS Date), NULL, 2)
INSERT [dbo].[Contract] ([ContractID], [Type], [Date], [EndDate], [PropertyID]) VALUES (2, N'Rented', CAST(N'2015-01-01' AS Date), CAST(N'2016-01-01' AS Date), 1)
INSERT [dbo].[Contract] ([ContractID], [Type], [Date], [EndDate], [PropertyID]) VALUES (3, N'Sold', CAST(N'2015-02-02' AS Date), NULL, 3)
INSERT [dbo].[Contract] ([ContractID], [Type], [Date], [EndDate], [PropertyID]) VALUES (4, N'Rented', CAST(N'2015-03-03' AS Date), CAST(N'2015-03-03' AS Date), 4)
INSERT [dbo].[Contract] ([ContractID], [Type], [Date], [EndDate], [PropertyID]) VALUES (5, N'Sold', CAST(N'2015-03-23' AS Date), NULL, 6)
SET IDENTITY_INSERT [dbo].[Contract] OFF
SET IDENTITY_INSERT [dbo].[Locations] ON 

INSERT [dbo].[Locations] ([LocationID], [Location]) VALUES (1, N'North')
INSERT [dbo].[Locations] ([LocationID], [Location]) VALUES (2, N'South')
INSERT [dbo].[Locations] ([LocationID], [Location]) VALUES (3, N'West')
INSERT [dbo].[Locations] ([LocationID], [Location]) VALUES (4, N'East')
SET IDENTITY_INSERT [dbo].[Locations] OFF
SET IDENTITY_INSERT [dbo].[Properties] ON 

INSERT [dbo].[Properties] ([PropertyID], [Type], [AddressLine1], [AddressLine2], [AddressLine3], [NoOfRooms], [NoOfBaths], [SquareMeters], [Parking], [Price], [Deposit], [Description], [SellerID], [LocationID], [PropertyType]) VALUES (1, N'To Rent', N'23 Fake Street', N'Dublin ', N'Ireland', 3, 2, 80, 1, 1400.0000, 1400.0000, N'A most appealing three bedroom detached house with a bright and spacious interior and which enjoys an enviable cul-de-sac location and with easily managed private gardens to front and rear and also has the benefit of off street parking.', 1, 1, N'House')
INSERT [dbo].[Properties] ([PropertyID], [Type], [AddressLine1], [AddressLine2], [AddressLine3], [NoOfRooms], [NoOfBaths], [SquareMeters], [Parking], [Price], [Deposit], [Description], [SellerID], [LocationID], [PropertyType]) VALUES (2, N'For Sale', N'12 Elm Street', N'Dublin', N'Ireland', 2, 1, 70, 0, 150000.0000, NULL, NULL, 2, 2, N'Apartement')
INSERT [dbo].[Properties] ([PropertyID], [Type], [AddressLine1], [AddressLine2], [AddressLine3], [NoOfRooms], [NoOfBaths], [SquareMeters], [Parking], [Price], [Deposit], [Description], [SellerID], [LocationID], [PropertyType]) VALUES (3, N'For Sale', N'123 Hard Street', N'Dublin', N'Ireland', 3, 1, 90, 1, 200000.0000, NULL, NULL, 3, 3, N'House')
INSERT [dbo].[Properties] ([PropertyID], [Type], [AddressLine1], [AddressLine2], [AddressLine3], [NoOfRooms], [NoOfBaths], [SquareMeters], [Parking], [Price], [Deposit], [Description], [SellerID], [LocationID], [PropertyType]) VALUES (4, N'To Rent', N'3 No Street', N'Dublin', NULL, 1, 1, 40, 0, 600.0000, 600.0000, NULL, 4, 4, N'Studio')
INSERT [dbo].[Properties] ([PropertyID], [Type], [AddressLine1], [AddressLine2], [AddressLine3], [NoOfRooms], [NoOfBaths], [SquareMeters], [Parking], [Price], [Deposit], [Description], [SellerID], [LocationID], [PropertyType]) VALUES (5, N'To Rent', N'5 Yes Street', N'Dublin', NULL, 2, 1, 65, 0, 900.0000, 900.0000, NULL, 5, 1, N'Apartement')
INSERT [dbo].[Properties] ([PropertyID], [Type], [AddressLine1], [AddressLine2], [AddressLine3], [NoOfRooms], [NoOfBaths], [SquareMeters], [Parking], [Price], [Deposit], [Description], [SellerID], [LocationID], [PropertyType]) VALUES (6, N'For Sale', N'6 Fake Lane', N'Dublin', NULL, 2, 1, 55, 0, 850.0000, 850.0000, NULL, 6, 2, N'Apartement')
INSERT [dbo].[Properties] ([PropertyID], [Type], [AddressLine1], [AddressLine2], [AddressLine3], [NoOfRooms], [NoOfBaths], [SquareMeters], [Parking], [Price], [Deposit], [Description], [SellerID], [LocationID], [PropertyType]) VALUES (7, N'To Rent', N'7 Elm Street', N'Dublin', NULL, 3, 1, 80, 0, 1500.0000, 1500.0000, NULL, 7, 2, N'House')
SET IDENTITY_INSERT [dbo].[Properties] OFF
SET IDENTITY_INSERT [dbo].[Sellers] ON 

INSERT [dbo].[Sellers] ([SellerID], [FirstName], [LastName], [Phone], [Email], [AgentID]) VALUES (1, N'Queen', N'Latifah', 5567788, N'QL@gmail.com', 9)
INSERT [dbo].[Sellers] ([SellerID], [FirstName], [LastName], [Phone], [Email], [AgentID]) VALUES (2, N'George', N'Clooney', 7238290, N'GC@gmail.com', 10)
INSERT [dbo].[Sellers] ([SellerID], [FirstName], [LastName], [Phone], [Email], [AgentID]) VALUES (3, N'Bruce', N'Willis', 7348389, N'BW@gmail.com', 11)
INSERT [dbo].[Sellers] ([SellerID], [FirstName], [LastName], [Phone], [Email], [AgentID]) VALUES (4, N'Susan', N'Sarandon', 5678789, N'SS@gmail.com', 12)
INSERT [dbo].[Sellers] ([SellerID], [FirstName], [LastName], [Phone], [Email], [AgentID]) VALUES (5, N'Sandra', N'Bullock', 7234682, N'SB@gmail.com', 13)
INSERT [dbo].[Sellers] ([SellerID], [FirstName], [LastName], [Phone], [Email], [AgentID]) VALUES (6, N'Ozzy', N'Osborne', 7662482, N'OO@gmail.com', 14)
INSERT [dbo].[Sellers] ([SellerID], [FirstName], [LastName], [Phone], [Email], [AgentID]) VALUES (7, N'Peter', N'Gabriell', 7638473, N'PG@gmail.com', 10)
SET IDENTITY_INSERT [dbo].[Sellers] OFF
ALTER TABLE [dbo].[Agents]  WITH CHECK ADD FOREIGN KEY([LocationID])
REFERENCES [dbo].[Locations] ([LocationID])
GO
ALTER TABLE [dbo].[Buyers]  WITH CHECK ADD FOREIGN KEY([AgentID])
REFERENCES [dbo].[Agents] ([AgentID])
GO
ALTER TABLE [dbo].[Contract]  WITH CHECK ADD FOREIGN KEY([PropertyID])
REFERENCES [dbo].[Properties] ([PropertyID])
GO
ALTER TABLE [dbo].[Properties]  WITH CHECK ADD FOREIGN KEY([LocationID])
REFERENCES [dbo].[Locations] ([LocationID])
GO
ALTER TABLE [dbo].[Properties]  WITH CHECK ADD FOREIGN KEY([SellerID])
REFERENCES [dbo].[Sellers] ([SellerID])
GO
ALTER TABLE [dbo].[Sellers]  WITH CHECK ADD FOREIGN KEY([AgentID])
REFERENCES [dbo].[Agents] ([AgentID])
GO
/****** Object:  StoredProcedure [dbo].[uspAgentLocation]    Script Date: 13/05/2016 11:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspAgentLocation]
@Location nvarchar (50)
AS
SELECT l.Location, COUNT(a.AgentID) AS [No Of Agents]
FROM Agents AS a, Locations AS l
WHERE a.LocationID = l.LocationID
GROUP BY l.Location
HAVING l.Location =@Location

GO
/****** Object:  StoredProcedure [dbo].[uspAvgPriceType]    Script Date: 13/05/2016 11:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspAvgPriceType]
@Type nvarchar (50)
AS
SELECT AVG(Price) AS [Avg Price]
FROM Properties
WHERE Type = @Type

GO
/****** Object:  StoredProcedure [dbo].[uspContractBetween]    Script Date: 13/05/2016 11:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspContractBetween]
@StartDate date,
@EndDate date
AS
SELECT *
FROM Contract
WHERE Date BETWEEN @StartDate AND @EndDate

GO
/****** Object:  StoredProcedure [dbo].[uspPriceLessThanBetweenDates]    Script Date: 13/05/2016 11:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspPriceLessThanBetweenDates]
@Price money,
@StartDate date,
@EndDate date
AS
SELECT p.Price, c.Type, c.Date
FROM Contract AS c, Properties AS p
WHERE c.PropertyID = p.PropertyID AND c.Date BETWEEN @StartDate AND @EndDate AND p.Price < @Price
GROUP BY c.Date, c.Type, p.Price

GO
/****** Object:  StoredProcedure [dbo].[uspSearchForLastNameEndsWithSomethingFromSellers]    Script Date: 13/05/2016 11:07:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspSearchForLastNameEndsWithSomethingFromSellers]
@something nvarchar(50)
AS
SELECT *
FROM Sellers
WHERE LastName LIKE '%' + @something

GO
